package com.jo.borrowing.models;


import org.springframework.data.repository.query.Param;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Entity
@Table(name = "borrowing_service_map",  schema = "room-mates")
@XmlRootElement
@NamedQueries({

        @NamedQuery(name = "BorrowingServiceMap.findByName", query = "SELECT c FROM BorrowingServiceMap c WHERE c.lender = :lender"),
        @NamedQuery(name = "BorrowingServiceMap.findByLenderAndBorrower", query = "SELECT b FROM BorrowingServiceMap b WHERE b.lender = :lender AND b.borrower=:borrower"),
        @NamedQuery(name = "BorrowingServiceMap.findByBorrowerAndLender", query = "SELECT b FROM BorrowingServiceMap b WHERE b.borrower = :borrower AND b.lender=:lender"),
        @NamedQuery(name = "BorrowingServiceMap.calculateOwed", query = "SELECT b FROM BorrowingServiceMap b WHERE b.lender = :lender AND b.borrower=:borrower"),

})
public class BorrowingServiceMap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "borrower")
    private String borrower;

    @Column(name = "lender")
    private String lender;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBorrower() {
        return borrower;
    }

    public void setBorrower(String borrower) {
        this.borrower = borrower;
    }

    public String getLender() {
        return lender;
    }

    public void setLender(String lender) {
        this.lender = lender;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public String toString() {
        return "BorrowingServiceMap{" +
                "id=" + id +
                ", borrower='" + borrower + '\'' +
                ", lender='" + lender + '\'' +
                ", amount=" + amount +
                ", createdOn=" + createdOn +
                '}';
    }
}
