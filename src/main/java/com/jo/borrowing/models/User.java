package com.jo.borrowing.models;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Entity
@Table(name = "users",  schema = "room-mates")
@XmlRootElement
@NamedQueries({

        @NamedQuery(name = "User.findAllExceptRequestName", query = "SELECT c FROM User c WHERE c.name != :name"),
        @NamedQuery(name = "User.findByRequestName", query = "SELECT c FROM User c WHERE c.name = :name"),
})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "room_mate_id")
    private Integer roomMateId;

    @Column(name = "name")
    private String name;

    @Column(name = "total_owes")
    private Double total_owes;

    @Column(name = "total_owed")
    private Double total_owed;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    public Integer getRoomMateId() {
        return roomMateId;
    }

    public void setRoomMateId(Integer roomMateId) {
        this.roomMateId = roomMateId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getTotal_owes() {
        return total_owes;
    }

    public void setTotal_owes(Double total_owes) {
        this.total_owes = total_owes;
    }

    public Double getTotal_owed() {
        return total_owed;
    }

    public void setTotal_owed(Double total_owed) {
        this.total_owed = total_owed;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
}
