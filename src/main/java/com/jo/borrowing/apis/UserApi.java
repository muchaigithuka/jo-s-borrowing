package com.jo.borrowing.apis;


import com.google.gson.JsonObject;
import com.jo.borrowing.apiobjects.IOUObject;
import com.jo.borrowing.models.BorrowingServiceMap;
import com.jo.borrowing.models.User;
import com.jo.borrowing.repository.BorrowingServiceMapRepository;
import com.jo.borrowing.repository.UserRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.NamedQuery;
import java.util.*;

@RestController
public class UserApi {

    @Autowired
    UserRepository userRepository;

    @Autowired
    BorrowingServiceMapRepository borrowingServiceMapRepository;

    @RequestMapping(value = "/add")
    public Optional<User> createUser(@RequestBody User user){

        if(user !=null){
            User newUser = new User();
            newUser.setName(user.getName());
            newUser.setCreatedOn(new Date());
            newUser.setTotal_owes(user.getTotal_owes());
            newUser.setTotal_owed(user.getTotal_owed());
            userRepository.save(newUser);
            if(userRepository.save(newUser) !=null){
                System.out.println("!!  SAVED !!");
            };
            Optional<User> addedUser = userRepository.findById(newUser.getRoomMateId());
            return addedUser;
        }

        return null;
    }

    @RequestMapping(value = "/users")
    public String findAllUsers(){

        final  org.json.JSONObject appUsers = new JSONObject();
        final  org.json.JSONArray jsonArray = new JSONArray();

        final List<String> json = new ArrayList<String>();

        List<User> listOfUsers = userRepository.findAll();
        listOfUsers.forEach( (user) ->{
            String name = user.getName();
            int key = user.getRoomMateId();
            json.add(user.getName());

        });

        appUsers.put("users" , "users");
        appUsers.put("users", json);

        return appUsers.toString();
    }

    @RequestMapping(value = "/iou")
    public String createIOU(@RequestBody IOUObject iouObject){

        if( iouObject.getAmount() !=null &&  iouObject.getLender() !=null && iouObject.getBorrower() !=null ){

            String lender = iouObject.getLender();
            String borrower = iouObject.getBorrower();

            //exempt the requesting user.getLender from all queries
            List<User> users = userRepository.findAllExceptRequestName(lender);

            User lending = userRepository.findByRequestName(lender);
            User borrowing = userRepository.findByRequestName(borrower);


            final org.json.JSONObject userObject = new JSONObject();

            if( users != null && lending !=null && borrowing !=null){

                //persist the request
                BorrowingServiceMap bsm = new BorrowingServiceMap();
                bsm.setBorrower(iouObject.getBorrower());
                bsm.setLender(iouObject.getLender());
                bsm.setAmount(iouObject.getAmount());
                bsm.setCreatedOn(new Date());
                borrowingServiceMapRepository.save(bsm);

                if( borrowingServiceMapRepository.save(bsm) != null){
                    System.out.println("Service Map Saved !!==");
                    //credit and debit lender and borrower

                    Double owed = lending.getTotal_owed();
                    Double addThis = iouObject.getAmount();
                    Double totalOwed = owed + addThis;
                    lending.setTotal_owed(totalOwed);
                    userRepository.save(lending);

                    System.out.println("Lender Credited");

                    Double owes = borrowing.getTotal_owes();
                    Double addNewAmount = iouObject.getAmount();
                    Double totalOwes = owes + addNewAmount;
                    borrowing.setTotal_owes(totalOwes);
                    userRepository.save(borrowing);

                    System.out.println("Borrower Debited");

                }
               //get name of user in request object
//                String name = userInRequestPayload.getName();
                //Borrower owes Lender
                Map<String , Double> kv = new HashMap<String , Double>();
                //Lender Owes Borrower
                Map<String , Double> kvv = new HashMap<String , Double>();
                //IOUOBJECT IS THE LENDER
               users.forEach((user) -> {
                   List<Double> llpq = new ArrayList<>();

                           List<BorrowingServiceMap> borrowerOwesLender = borrowingServiceMapRepository.findByLenderAndBorrower(iouObject.getLender(), user.getName());
                           borrowerOwesLender.forEach((f) -> {
                               llpq.add(f.getAmount());
                           });
                           Double sum = llpq.stream().mapToDouble(Double::doubleValue).sum();

//                           System.out.println(sum);
                           kv.put(user.getName(), sum);
               });
               //IOUOBJECT IS THE NOW BORROWER
               users.forEach((user) -> {

                   List<Double> lq = new ArrayList<>();

                   List<BorrowingServiceMap> lenderOwesBorrower = borrowingServiceMapRepository.findByBorrowerAndLender(iouObject.getLender() , user.getName());

                     lenderOwesBorrower.forEach((c) ->{

                         lq.add(c.getAmount());

                     });

                   Double sumq = lq.stream().mapToDouble(Double::doubleValue).sum();
//                   System.out.println(sumq);

                   kvv.put(user.getName() , sumq);

               });

                 System.out.println("`` " + iouObject.getLender()+  " Owes``````" + "\n");

                for (Map.Entry<String, Double> pair : kvv.entrySet()) {
                    System.out.println( pair.getKey() + " --> " + pair.getValue());
                }

                System.out.println("```` " + iouObject.getLender()+  "  Owed_by````````" + "\n");

                for (Map.Entry<String, Double> pair : kv.entrySet()) {
                    System.out.println( pair.getKey() + " ==> " + pair.getValue());
                }

                //CALCULATE THE BALANACE FROM THE UPDATED ACCOUNTS
                Double balance =  lending.getTotal_owed() - lending.getTotal_owes();
                userObject.put("name" , iouObject.getLender());
                userObject.put("owed_by" , kv);
                userObject.put("owes" , kvv);
                userObject.put("balance" , balance);
                return userObject.toString();
            }else{
                return "REQUEST COULD NOT BE COMPLETED , CHECK IF BORROWER / LENDER EXISTS";
            }


        }
        return "IOU REQUEST COULD NOT BE COMPLETED";
    }

}

