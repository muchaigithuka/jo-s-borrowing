CREATE TABLE `users` (
  `room_mate_id` int(191) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL DEFAULT 'USER',
  `total_owes` double NOT NULL DEFAULT '0',
  `total_owed` double NOT NULL DEFAULT '0',
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`room_mate_id`)
);


CREATE TABLE `borrowing_service_map` (
  `id` int(191) unsigned NOT NULL AUTO_INCREMENT,
  `borrower` varchar(191) NOT NULL DEFAULT 'BORROWER',
  `lender` varchar(191) NOT NULL DEFAULT 'LENDER',
  `amount` double NOT NULL DEFAULT '0',
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);